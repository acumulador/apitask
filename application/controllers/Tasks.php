<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH.'/libraries/REST_Controller.php');

class Tasks extends Rest_controller {

	public function __construct()
	{
		header("Access-Control-Allow-Methods: PUT, GET, POST, DELETE, OPTIONS");
		header("Access-Control-Allow-Headers: Content-Type, Content-Length, Accept-Encoding");
		header("Access-Control-Allow-Origin: *");

		parent::__construct();
		$this->load->model('Tasks_model');
		
	}
	
	public function all_get($pag=0)
	{
		//Todos las tareas
		$tasks = $this->Tasks_model->get($pag);

		if (!is_null($tasks)) 
		{
			$this->response(array('tasks' => $tasks), 200);
		}
		else
		{
			$this->response(array('error' => 'No se encuentran tareas en la base de datos...'), 404);
		}
		
	}
	
	public function active_get($estado='')
	{
		$taskactive = $this->Tasks_model->search_active($pag=0);

		if (!is_null($taskactive))
		{
			$this->response(array('tasks_active' => $taskactive), 200);
		}
		else
		{
			$this->response(array('error' => 'Tareas no encontradas...'), 404);
		}
	}

	public function find_get($id=0)
	{
		$task = $this->Tasks_model->search($id);

		if (!is_null($task))
		{
			$this->response(array('task' => $task), 200);
		}
		else
		{
			$this->response(array('error' => 'Tarea no encontrada...'), 404);
		}
	}

	public function index_post()
	{
		//Crea		
		$id = $this->Tasks_model->save();

		if (!is_null($id)) 
		{
			$this->response(array('response' => $id), 200);
		}
		else
		{
			$this->response(array('error' => 'Error en el servidor, No se agrego la tarea...'), 404);
		}
	}

	public function index_put($id)
	{
		//Actualiza
		if (!($this->post('task')) || !$id) 
		{
			$this->response(array('error' => null), 404);
		}

		$update = $this->Tasks_model->update($id, $this->post('task'));

		if (!is_null($update)) 
		{
			$this->respondse(array('response' => 'Tarea actualizada correctamente'), 200);
		}
		else
		{
			$this->response(array('error' => 'Error en el servidor, No se actualizar la tarea...'), 404);
		}
	}

	public function index_delete($id)
	{
		//Elimina
		if (!$id) 
		{
			$this->response(array('error' => null), 404);
		}

		$del = $this->Tasks_model->delete($id);

		if (!is_null($del)) 
		{
			$this->respondse(array('response' => 'Tarea eliminado correctamente'), 200);
		}
		else
		{
			$this->response(array('error' => 'Error en el servidor, No se eliminó la tarea...'), 404);
		}
	}
}
