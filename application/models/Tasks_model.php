<?php


class Tasks_model extends CI_Model
{
	
	public function __construct()
	{
		parent::__construct();
	}

	function get($pag)
	{
		$pag = $pag * 10;
		$query = $this->db->select('*')->from('tbl_tasks')->order_by('id', 'ASC')->limit('10', $pag)->get();

		if ($query->num_rows() > 0) 
		{
			return $query->result_array();
		}
		else
		{
			return null;
		}
	}
	
	function search_active($pag)
	{
		$pag = $pag * 10;
		$query = $this->db->select('*')->from('tbl_tasks')->where('estado', 'a')->order_by('id', 'ASC')->limit('10', $pag)->get();

		if ($query->num_rows() > 0) 
		{
			return $query->result_array();
		}
		else
		{
			return null;
		}
	}

	function search($id)
	{
		if ($id != 0)
		{
			$query = $this->db->select('*')->from('tbl_tasks')->where('id', $id)->get();

			if ($query->num_rows() === 1) 
			{
				return $query->row_array();
			}
			else
			{
				return "No se encuentran registros con el id indicado";
			}
		}
		else
		{
			return "No se ingreso id para consultar";
		}
	}

	public function save()
	{
		$data = array(
	         'ds_task' => $this->input->post('ds_task'),
	         'estado' => 'a'
	    );

	    $this->db->insert('tbl_tasks',$data);

		if ($this->db->affected_rows() === 1)
		{
			return $this->db->insert_id();
		}
		else
		{
			return false;
		}
	}

	// public function save($producto)
	// {
	// 	$this->db->set($this->_setProduct($producto))->insert('tbl_productos');

	// 	if ($this->db->affected_rows() === 1)
	// 	{
	// 		return $this->db->insert_id();
	// 	}
	// 	else
	// 	{
	// 		return false;
	// 	}
	// }

	public function update($id, $task)
	{
		$this->db->set($this->_setTask($task))->update('tbl_tasks');

		if ($this->db->affected_rows() === 1)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public function delete($id)
	{
		$this->db->where('id', $id)->delete('tbl_tasks');

		if ($this->db->affected_rows() === 1)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	/*private function _setProductJSon($producto)
	{
		$arrProducto = json_decode($producto);

		return $arrProducto(
			'id' = $arrProducto['id'],
			'titulo_producto' = $arrProducto['titulo_producto'],
			'ds_producto' = $arrProducto['ds_producto'],
			'foto' = $arrProducto['foto']
		);
	}*/

	private function _setProduct($producto)
	{
		return array(
			'id' => $task['id'],
			'ds_task' => $task['ds_task'],
			'estado' => $task['estado']
		);
	}
	
}